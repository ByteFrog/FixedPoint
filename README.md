# FixedPoint

A C++ header-only library for fixed-point numbers. Includes common mathematical operations based on the ones found in cmath for floating-point types, as well as a test suite using gtest.

The code is based on a fantastic blog post by PeterSchregle that can be found [here](https://www.codeproject.com/articles/37636/fixed-point-class). The code has been modified in places to compile under GCC.

## Motivation

When I wrote the first version of the RetroFrog 2D game engine, one of the challenges I faced was how to manage low velocities in a system that ran at 30 FPS. For games with low resolutions, even a speed of 1 was fast because pixels were so large. I ended up hacking together a sub-pixel system that worked, but was ugly and required a decent amount of code for edge cases. It was great practice, but I knew a similar problem was going to show up in the second version.

I stumbled on to the idea of fixed-point types while exploring the technical limitations of Minecraft and realized a good fixed-point library could be useful for a lot of my game-related projects. Sure, I could have downloaded someone else's, but where's the fun in that? Writing my own fixed-point library seemed like a fun and interesting learning project, so here we are.

## Requirements

The header has no external dependencies outside the C++ standard libraries.

[Google Test](https://github.com/google/googletest) is required to compile the test cases contained in test.cpp.

## Usage

This is a header-only library. Simply add the header to your project and you're good to go.

## Known Bugs

* sin() and cos() functions become increasingly inaccurate as the fractional bits decrease, especially when the input value is close to PI.

## Credits

A huge thanks to PeterSchregle for the original blog post that served as the basis for this project.
