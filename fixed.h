/*
 * fixed.h
 *
 *  Created on: Sep. 30, 2020
 *      Author: Aelia Virdaeus
 *
 *  A template class representing a fixed-point precision value.
 *
 *  Notes:
 *  - The Eclipse IDE gets confused when using the less-than operator to compare the fixed type and flags the statements
 *    as an error even though it compiles fine. It was an annoyance when looking for real errors, so I just wrapped the
 *    fixed declarations in brackets to make Eclipse happy.
 *
 */

#pragma once

#include <cerrno>
#include <cmath>	//M_PI
#include <limits>
#include <utility>

namespace fpml {

namespace _impl {

/* These two templates take advantage of template recursion to generate the necessary powers of
 * 2 at compile time instead of at runtime.
 */
template<int F>
struct power2 {
	static const long long value = 2 * power2<F-1>::value;
};

template<>
struct power2<0> {
	static const long long value = 1;
};

/* The following templates are used to promote the underlying type to one that is twice
 * as wide in order to avoid overflowing during multipliction and division of values.
 *
 * To allow for better portability, the promotions are based on the C++ standard. This
 * means we assume the number of bits are always the minimum, not the number the system
 * might actually use. This is why both the short type and the int type, which are both
 * specified as having at least 16 bits under the C++ standard, promote to the long int
 * type (32 bits).
 */
template<typename T = signed char>
struct promote_type {
	typedef signed short int type;
};

template<>
struct promote_type<signed short int> {
	typedef signed long int type;
};

template<>
struct promote_type<signed int> {
	typedef signed long int type;
};

template<>
struct promote_type<signed long int> {
	typedef signed long long int type;
};

template<>
struct promote_type<unsigned char> {
	typedef unsigned short int type;
};

template<>
struct promote_type<unsigned short int> {
	typedef unsigned long int type;
};

template<>
struct promote_type<unsigned int> {
	typedef unsigned long int type;
};

template<>
struct promote_type<unsigned long int> {
	typedef unsigned long long int type;
};

}

template<typename B, unsigned char I, unsigned char F = std::numeric_limits<B>::digits - I>
class fixed {

public:
	template<typename B2, unsigned char I2, unsigned char F2>
	friend class fixed;

	fixed() { }
	fixed(const fixed<B, I, F>& rhs) : value_(rhs.value_) { }
	template<unsigned char I2, unsigned char F2>
	fixed(const fixed<B, I2, F2>& rhs) : value_(rhs.value_) {
		/* We don't need to worry about the case where I and F are the same values as the current
		 * object because there's a specific constructor for that which should be called instead.
		 * This should only be called if they differ.
		 */
		if(I-I2 > 0) value_ >>= I-I2;
		else value_ <<= I2-I;
	}

	template<typename T>
	fixed(T value) : value_(static_cast<B>(value << F)) { }
	fixed(float value) : value_(static_cast<B>(value * _impl::power2<F>::value)) { }
	fixed(double value) : value_(static_cast<B>(value * _impl::power2<F>::value)) { }
	fixed(long double value) : value_(static_cast<B>(value * _impl::power2<F>::value)) { }

	template<typename T>
	operator T() const {
		return value_ >> F;
	}
	operator float() const {
		return static_cast<float>(value_) / _impl::power2<F>::value;
	}
	operator double() const {
		return static_cast<double>(value_) / _impl::power2<F>::value;
	}
	operator long double() const {
		return static_cast<long double>(value_) / _impl::power2<F>::value;
	}
	operator bool() const {
		return static_cast<bool>(value_);
	}

	fixed<B, I, F>& operator=(fixed<B, I, F> rhs) {
		swap(*this, rhs);
		return *this;
	}

	template<unsigned char I2, unsigned char F2>
	fixed<B, I, F>& operator=(fixed<B, I2, F2>& rhs) {
		swap(*this, rhs);
		return *this;
	}

	fixed<B, I, F> operator-() const {
		fixed<B, I, F> result;
		result.value_ = -value_;
		return result;
	}

	fixed<B, I, F>& operator++() {
		value_ += _impl::power2<F>::value;
		return *this;
	}

	fixed<B, I, F>& operator--() {
		value_ -= _impl::power2<F>::value;
		return *this;
	}

	fixed<B, I, F> operator++(int) {
		fixed<B, I, F> temp(*this);
		++temp;
		return temp;
	}

	fixed<B, I, F> operator--(int) {
		fixed<B, I, F> temp(*this);
		--temp;
		return temp;
	}

	fixed<B, I, F>& operator+=(const fixed<B, I, F>& rhs) {
		value_ += rhs.value_;
		return *this;
	}

	fixed<B, I, F>& operator-=(const fixed<B, I, F>& rhs) {
		value_ -= rhs.value_;
		return *this;
	}

	fixed<B, I, F> operator+(const fixed<B, I, F>& rhs) {
		fixed<B, I, F> temp(*this);
		temp += rhs;
		return temp;
	}

	fixed<B, I, F> operator-(const fixed<B, I, F>& rhs) {
		fixed<B, I, F> temp(*this);
		temp -= rhs;
		return temp;
	}

	fixed<B, I, F>& operator*=(const fixed<B, I, F>& rhs) {
		value_ = (static_cast<typename _impl::promote_type<B>::type>(value_) * rhs.value_) >> F;
		return *this;
	}

	fixed<B, I, F>& operator/=(const fixed<B, I, F>& rhs) {
		value_ = (static_cast<typename _impl::promote_type<B>::type>(value_) << F) / rhs.value_;
		return *this;
	}

	fixed<B, I, F> operator*(const fixed<B, I, F>& rhs) {
		fixed<B, I, F> temp(*this);
		temp *= rhs;
		return temp;
	}

	fixed<B, I, F> operator/(const fixed<B, I, F>& rhs) {
		fixed<B, I, F> temp(*this);
		temp /= rhs;
		return temp;
	}

	fixed<B, I, F>& operator>>=(size_t shift) {
		value_ >>= shift;
		return *this;
	}

	fixed<B, I, F>& operator<<=(size_t shift) {
		value_ <<= shift;
		return *this;
	}

	fixed<B, I, F> operator>>(size_t shift) {
		fixed<B, I, F> temp(*this);
		temp >>= shift;
		return temp;
	}

	fixed<B, I, F> operator<<(size_t shift) {
		fixed<B, I, F> temp(*this);
		temp <<= shift;
		return temp;
	}

	bool operator==(const fixed<B, I, F>& rhs) const {
		return value_ == rhs.value_;
	}

	bool operator!=(const fixed<B, I, F>& rhs) const {
		return !(*this == rhs);
	}

	bool operator<(const fixed<B, I, F>& rhs) const {
		return value_ < rhs.value_;
	}

	bool operator>(const fixed<B, I, F>& rhs) const {
		return rhs < *this;
	}

	bool operator<=(const fixed<B, I, F>& rhs) const {
		return !(*this > rhs);
	}

	bool operator>=(const fixed<B, I, F>& rhs) const {
		return !(*this < rhs);
	}

	bool operator !() const {
		return value_ == 0;
	}

	friend void swap(fixed<B, I, F>& first, fixed<B, I, F>& second) {
		std::swap(first.value_, second.value_);
	}

	friend fixed<B, I, F> fabs(const fixed<B, I, F>& x) {
		return (x < (fixed<B, I, F>(0)) ? -x : x);
	}

	friend fixed<B, I, F> ceil(const fixed<B, I, F>& x) {
		fixed<B, I, F> result;
		result.value_ = x.value_ & ~(_impl::power2<F>::value-1);
		result += fixed<B, I, F>(x.value_ & (_impl::power2<F>::value-1) ? 1 : 0);
		return result;
	}

	friend fixed<B, I, F> floor(const fixed<B, I, F>& x) {
		fixed<B, I, F> result;
		result.value_ = x.value_ & ~(_impl::power2<F>::value-1);
		return result;
	}

	friend fixed<B, I, F> fmod(const fixed<B, I, F>& x, const fixed<B, I, F>& y) {
		fixed<B, I, F> result;
		result.value_ = x.value_ % y.value_;
		return result;
	}

	friend fixed<B, I, F> modf(const fixed<B, I, F>& x, fixed<B, I, F>* intpart) {
		fixed<B, I, F> integer;
		integer.value_ = x.value_ & ~(_impl::power2<F>::value-1);
		*intpart = x < (fixed<B, I, F>(0)) ? integer + fixed<B, I, F>(1) : integer;

		fixed<B, I, F> fraction;
		fraction.value_ = x.value_ & (_impl::power2<F>::value-1);
		if(x < (fixed<B, I, F>(0))) {
			fraction.value_ = fraction.value_ | ~(_impl::power2<F>::value-1);
		}
		return fraction;
	}

	/* Uses a Taylor series to approximate the result of sin().
	 * This function
	 */
	friend fixed<B, I, F> sin(const fixed<B, I, F>& x) {
		//Reduce x to be in the range -PI < x < PI
		fixed<B, I, F> x1 = fmod(x, fixed<B, I, F>(M_PI * 2));
		if(x1 > fixed<B, I, F>(M_PI)) {
			x1 -= fixed<B, I, F>(M_PI * 2);
		}

		//Maclaurin series expansion
		fixed<B, I, F> xx = x1 * x1;

		fixed<B, I, F> y = 0;
		//y += fixed<B, I, F>(1.0 / (9 * 8 * 7 * 6 * 5 * 4 * 3 * 2));
		//y *= xx;
		y -= fixed<B, I, F>(1.0 / (7 * 6 * 5 * 4 * 3 * 2));
		y *= xx;
		y += fixed<B, I, F>(1.0 / (5 * 4 * 3 * 2));
		y *= xx;
		y -= fixed<B, I, F>(1.0 / (3 * 2));
		y *= xx;
		y += fixed<B, I, F>(1);
		y *= x1;

		return y;
	}

	friend fixed<B, I, F> cos(const fixed<B, I, F>& x) {
		//Reduce x to be in the range -PI < x < PI
		fixed<B, I, F> x1 = fmod(x, fixed<B, I, F>(M_PI * 2));
		if(x1 > fixed<B, I, F>(M_PI)) {
			x1 -= fixed<B, I, F>(M_PI * 2);
		}

		//Maclaurin series expansion
		fixed<B, I, F> xx = x1 * x1;

		fixed<B, I, F> y = 0;
		y += fixed<B, I, F>(1.0 / (8 * 7 * 6 * 5 * 4 * 3 * 2));
		y *= xx;
		y -= fixed<B, I, F>(1.0 / (6 * 5 * 4 * 3 * 2));
		y *= xx;
		y += fixed<B, I, F>(1.0 / (4 * 3 * 2));
		y *= xx;
		y -= fixed<B, I, F>(1.0 / (2));
		y *= xx;
		y += fixed<B, I, F>(1);

		return y;
	}

	friend fixed<B, I, F> sqrt(const fixed<B, I, F>& x) {
		/* Because this library is intended to mimic cmath as closely as possible, we won't
		 * throw an exception for a negative value. We'll follow the same procedure as cmath
		 * and set the global variable errno to EDOM and return 0.
		 */
		if(x < (fixed<B, I, F>(0))) {
			errno = EDOM;
			return 0;
		}

		/* This algorithm uses digit-by-digit calculation of a square root applied to the binary
		 * numeral system. Running through the algorithm by hand seems to suggest that the
		 * algorithm doesn't work for fixed-point numbers, but the online resource I've been
		 * using online suggests that it does. If the wrong values come out, the algorithm is
		 * likely the problem.
		 */
		typename _impl::promote_type<B>::type n = static_cast<typename _impl::promote_type<B>::type>(x.value_) << (I - 1);
		typename _impl::promote_type<B>::type result = 0;
		typename _impl::promote_type<B>::type b = static_cast<typename _impl::promote_type<B>::type>(1) <<
				(std::numeric_limits<typename _impl::promote_type<B>::type>::digits - 1);
		while(b > n) {
			b >>= 2;
		}
		while(b != 0) {
			if(n >= result + b) {
				n = n - (result + b);
				result = result + (b << 1);
			}
			result >>= 1;
			b >>= 2;
		}
		fixed<B, I, F> root;
		root.value_ = static_cast<B>(result);
		return root;
	}

private:
	B value_;
};

}

namespace std {
	template<typename B, unsigned char I, unsigned char F>
	class numeric_limits<fpml::fixed<B, I, F>> {
	public:
		static constexpr bool is_specialized = true;
		static constexpr bool is_signed = numeric_limits<B>::is_signed;
		static constexpr bool is_integer = false;
		static constexpr bool is_exact = true;
		static constexpr bool has_infinity = false;
		static constexpr bool has_quiet_NaN = false;
		static constexpr bool has_signaling_NaN = false;
		static constexpr float_denorm_style has_denorm = denorm_absent;
		static constexpr bool has_denorm_loss = false;
		static constexpr float_round_style round_style = round_toward_zero;
		static constexpr bool is_iec559 = false;
		static constexpr bool is_bounded = true;
		static constexpr bool is_modulo = false;
		static constexpr int digits = I;
		static constexpr int digits10 = digits;
		static constexpr int max_digits10 = 0;
		static constexpr int radix = 2;
		static constexpr int min_exponent = 0;
		static constexpr int min_exponent10 = 0;
		static constexpr int max_exponent = 0;
		static constexpr int max_exponent10 = 0;
		//Traps is false because division is handled by the promoted type of the base type, not by
		//this class itself.
		static constexpr bool traps = false;
		static constexpr bool tinyness_before = false;
	};
}
