/*
 * test.cpp
 *
 *  Created on: Oct. 11, 2020
 *      Author: Aelia Virdaeus
 */

#include <cmath>			//M_PI, abs()
#include <gtest/gtest.h>
#include <iostream>
#include "fixed.h"

using namespace fpml;

/* Each test is scoped to avoid having to come up with new variable names for each test. This makes it easier to
 * move tests around to keep it organized without having to remember which variable names have already been used.
 */

TEST(ConstructionTests, DenseCharType) {

	float val;
	{
		fixed<char,3,4> fix((char)5);
		float val = fix;
		EXPECT_FLOAT_EQ(val, 5.0) << "Positive construction from char: unexpected value";
	}

	{
		fixed<char,3,4> fix((char)0);
		val = fix;
		EXPECT_FLOAT_EQ(val, 0.0) << "Zero construction from char: unexpected value";
	}

	{
		fixed<char,3,4> fix((char)-5);
		val = fix;
		EXPECT_FLOAT_EQ(val, -5.0) << "Negative construction from char: unexpected value";
	}

	{
		fixed<char,3,4> fix((short)1);
		val = fix;
		EXPECT_FLOAT_EQ(val, 1.0) << "Positive construction from short: unexpected value";
	}

	{
		fixed<char,3,4> fix((short)0);
		val = fix;
		EXPECT_FLOAT_EQ(val, 0.0) << "Zero construction from short: unexpected value";
	}

	{
		fixed<char,3,4> fix((short)-1);
		val = fix;
		EXPECT_FLOAT_EQ(val, -1.0) << "Negative construction from short: unexpected value";
	}

	{
		fixed<char,3,4> fix((float)6.75);
		val = fix;
		EXPECT_FLOAT_EQ(val, 6.75) << "Positive construction from float: unexpected value";
	}

	{
		fixed<char,3,4> fix((float)0.0);
		val = fix;
		EXPECT_FLOAT_EQ(val, 0.0) << "Zero construction from float: unexpected value";
	}

	{
		fixed<char,3,4> fix((float)-7.25);
		val = fix;
		EXPECT_FLOAT_EQ(val, -7.25) << "Negative construction from float: unexpected value";
	}

	{
		fixed<char,3,4> fix(7);
		val = fix;
		EXPECT_FLOAT_EQ(val, 7.0) << "Construction from implied integral type: unexpected value";
	}

	{
		fixed<char,3,4> fix(7.125);
		val = fix;
		EXPECT_FLOAT_EQ(val, 7.125) << "Construction from implied floating type: unexpected value";
	}

	{
		fixed<char,3,4> fix1(5.875);
		fixed<char,3,4> fix2(fix1);
		val = fix2;
		EXPECT_FLOAT_EQ(val, 5.875) << "Copy construction from equivalent type: unexpected value";
	}

	{
		fixed<char,2,5> fix1(1.0313);
		fixed<char,3,4> fix2(fix1);
		val = fix2;
		EXPECT_FLOAT_EQ(val, 1.0) << "Copy construction from smaller significand type: unexpected value";
	}

	{
		fixed<char,4,3> fix1(1.375);
		fixed<char,3,4> fix2(fix1);
		val = fix2;
		EXPECT_FLOAT_EQ(val, 1.375) << "Copy construction from bigger significand type: unexpected value";
	}

	{
		fixed<char,3,4> fix = 7;
		val = fix;
		EXPECT_FLOAT_EQ(val, 7.0) << "Copy construction via assignment from implied integral type: unexpected value";
	}

	{
		fixed<char,3,4> fix = 7.125;
		val = fix;
		EXPECT_FLOAT_EQ(val, 7.125) << "Copy construction via assignment from implied floating type: unexpected value";
	}

	{
		const char const_val = -5;
		fixed<char,3,4> fix(const_val);
		val = fix;
		EXPECT_FLOAT_EQ(val, -5) << "Copy construction via assignment from const char type: unexpected value";
	}

	{
		const float const_val = -3.75;
		fixed<char,3,4> fix(const_val);
		val = fix;
		EXPECT_FLOAT_EQ(val, -3.75) << "Copy construction via assignment from const float type: unexpected value";
	}
}

//OPERATORS

TEST(negativeOperatorTest, HandlesPositiveToNegative) {
	fixed<short,10,5> fix = 5.125;
	float val = -fix;
	EXPECT_FLOAT_EQ(val, -5.125);
}

TEST(negativeOperatorTest, HandlesNegativeToPositive) {
	fixed<short,10,5> fix = -4.375;
	float val = -fix;
	EXPECT_FLOAT_EQ(val, 4.375);
}

TEST(negativeOperatorTest, HandlesPositiveFraction) {
	fixed<short,10,5> fix = 0.125;
	float val = -fix;
	EXPECT_FLOAT_EQ(val, -0.125);
}

TEST(negativeOperatorTest, HandlesNegativeFraction) {
	fixed<short,10,5> fix = -0.125;
	float val = -fix;
	EXPECT_FLOAT_EQ(val, 0.125);
}

//COMPARISONS

TEST(equalityOperatorTest, HandlesPositive) {
	fixed<short,10,5> fix1 = 1.625;
	fixed<short,10,5> fix2 = 1.625;
	EXPECT_TRUE(fix1 == fix2);
}

TEST(equalityOperatorTest, HandlesNegative) {
	fixed<short,10,5> fix1 = -1.5;
	fixed<short,10,5> fix2 = -1.5;
	EXPECT_TRUE(fix1 == fix2);
}

TEST(equalityOperatorTest, HandlesZero) {
	fixed<short,10,5> fix1 = 0;
	fixed<short,10,5> fix2 = 0;
	EXPECT_TRUE(fix1 == fix2);
}

TEST(lessThanOperatorTest, HandlesPositiveDifferent) {
	fixed<short,10,5> fix1 = 2.5;
	fixed<short,10,5> fix2 = 3.635;
	EXPECT_TRUE(fix1 < fix2);
}

TEST(lessThanOperatorTest, HandlesNegativeDifferent) {
	fixed<short,10,5> fix1 = -4.5;
	fixed<short,10,5> fix2 = -3.635;
	EXPECT_TRUE(fix1 < fix2);
}

TEST(lessThanOperatorTest, HandlesPositiveNegativeDifferent) {
	fixed<short,10,5> fix1 = -2.5;
	fixed<short,10,5> fix2 = 3.635;
	EXPECT_TRUE(fix1 < fix2);
}

TEST(lessThanOperatorTest, HandlesAgainstZero) {
	fixed<short,10,5> fix1 = -0.125;
	fixed<short,10,5> zero(0);
	EXPECT_TRUE(fix1 < zero);
}

TEST(lessThanOperatorTest, HandlesEqual) {
	fixed<short,10,5> fix1 = 2.125;
	fixed<short,10,5> fix2 = 2.125;
	EXPECT_FALSE(fix1 < fix2);
}

//MATH OPERATIONS

TEST(fabsTest, HandlesPositiveInput) {
	fixed<short,7,8> fix = 12.03125;
	float val = fabs(fix);
	EXPECT_FLOAT_EQ(val, 12.03125);
}

TEST(fabsTest, HandlesNegativeInput) {
	fixed<short,7,8> fix = -12.03125;
	float val = fabs(fix);
	EXPECT_FLOAT_EQ(val, 12.03125);
}

TEST(fabsTest, HandlesZeroInput) {
	fixed<short,7,8> fix = 0;
	float val = fabs(fix);
	EXPECT_FLOAT_EQ(val, 0.0);
}

TEST(ceilTest, HandlesPositiveFraction) {
	fixed<short,7,8> fix = 126.09375;
	float val = ceil(fix);
	EXPECT_FLOAT_EQ(val, 127.0);
}

TEST(ceilTest, HandlesNegativeFraction) {
	fixed<short,7,8> fix = -65.09375;
	float val = ceil(fix);
	EXPECT_FLOAT_EQ(val, -65.0);
}

TEST(ceilTest, HandlesPositiveWhole) {
	fixed<short,7,8> fix = 70.0;
	float val = ceil(fix);
	EXPECT_FLOAT_EQ(val, 70.0);
}

TEST(ceilTest, HandlesNegativeWhole) {
	fixed<short,7,8> fix = -45.0;
	float val = ceil(fix);
	EXPECT_FLOAT_EQ(val, -45.0);
}

TEST(floorTest, HandlesPositiveFraction) {
	fixed<short,7,8> fix = 126.09375;
	float val = floor(fix);
	EXPECT_FLOAT_EQ(val, 126.0);
}

TEST(floorTest, HandlesNegativeFraction) {
	fixed<short,7,8> fix = -65.09375;
	float val = floor(fix);
	EXPECT_FLOAT_EQ(val, -66.0);
}

TEST(floorTest, HandlesPositiveWhole) {
	fixed<short,7,8> fix = 70.0;
	float val = floor(fix);
	EXPECT_FLOAT_EQ(val, 70.0);
}

TEST(floorTest, HandlesNegativeWhole) {
	fixed<short,7,8> fix = -45.0;
	float val = floor(fix);
	EXPECT_FLOAT_EQ(val, -45.0);
}

TEST(fmodTest, HandlesPositiveFraction) {
	fixed<short,7,8> fix = 27.5;
	fixed<short,7,8> mod = 5.0;
	float val = fmod(fix, mod);
	EXPECT_FLOAT_EQ(val, 2.5);
}

TEST(fmodTest, HandlesNegativeFraction) {
	fixed<short,7,8> fix = -27.5;
	fixed<short,7,8> mod = 5.0;
	float val = fmod(fix, mod);
	EXPECT_FLOAT_EQ(val, -2.5);
}

TEST(modfTest, HandlesPositive) {
	fixed<short,10,5> fix = 250.125;
	fixed<short,10,5> intpart;
	fix = modf(fix, &intpart);
	float val = intpart;
	EXPECT_FLOAT_EQ(val, 250.0) << "Unexpected integer portion";
	val = fix;
	EXPECT_FLOAT_EQ(val, 0.125) << "Unexpected fractional portion";
}

TEST(modfTest, HandlesNegative) {
	fixed<short,10,5> fix = -250.125;
	fixed<short,10,5> intpart;
	fix = modf(fix, &intpart);
	float val = intpart;
	EXPECT_FLOAT_EQ(val, -250.0) << "Unexpected integer portion";
	val = fix;
	EXPECT_FLOAT_EQ(val, -0.125) << "Unexpected fractional portion";
}

TEST(modfTest, HandlesWhole) {
	fixed<short,10,5> fix = 100.0;
	fixed<short,10,5> intpart;
	fix = modf(fix, &intpart);
	float val = intpart;
	EXPECT_FLOAT_EQ(val, 100.0) << "Unexpected integer portion";
	val = fix;
	EXPECT_FLOAT_EQ(val, 0.0) << "Unexpected fractional portion";
}

/* Because sin() and cos() are approximations, trying to compare against specific values wouldn't work very well.
 * Instead, it makes more sense to ensure the computed value sits within a defined range of the expected value.
 */
const float trigTolerance = 0.1;
const float trigOutputs[9] = { -1.0, -0.9239, -0.7071, -0.3827, 0.0, 0.3827, 0.7071, 0.9239, 1.0 };

TEST(sinTest, LongMiddle) {
	fixed<long,15,16> fix;
	float val, diff;
	bool forward = true;

	int j = 4;
	for(int i = 0; i < 16; ++i) {
		fix = (i * M_PI) / 8;
		val = sin(fix);
		diff = trigOutputs[j] - val;
		EXPECT_TRUE(fabs(diff) < trigTolerance) << "Output exceeds tolerance for " << i << "PI/16: " << val << "/" << trigOutputs[j] << " (" << diff << ")";
		if(j == 8 || j == 0) {
			forward = !forward;
		}
		if(forward) {
			++j;
		}
		else {
			--j;
		}
	}
}

TEST(sinTest, LongLeft) {
	fixed<long,1,30> fix;
	float val, diff;
	bool forward = true;

	int j = 4;
	for(int i = 0; i < 16; ++i) {
		fix = (i * M_PI) / 8;
		val = sin(fix);
		diff = trigOutputs[j] - val;
		EXPECT_TRUE(fabs(diff) < trigTolerance) << "Output exceeds tolerance for " << i << "PI/16: " << val << "/" << trigOutputs[j] << " (" << diff << ")";
		if(j == 8 || j == 0) {
			forward = !forward;
		}
		if(forward) {
			++j;
		}
		else {
			--j;
		}
	}
}

TEST(sinTest, LongRight) {
	fixed<long,30,1> fix;
	float val, diff;
	bool forward = true;

	int j = 4;
	for(int i = 0; i < 16; ++i) {
		fix = (i * M_PI) / 8;
		val = sin(fix);
		diff = trigOutputs[j] - val;
		EXPECT_TRUE(fabs(diff) < trigTolerance) << "Output exceeds tolerance for " << i << "PI/16: " << val << "/" << trigOutputs[j] << " (" << diff << ")";
		if(j == 8 || j == 0) {
			forward = !forward;
		}
		if(forward) {
			++j;
		}
		else {
			--j;
		}
	}
}

TEST(sinTest, WrappingValues) {
	fixed<long,15,16> fix;
	float val, diff;
	bool forward = true;

	int j = 4;
	for(int i = 0; i < 16000; ++i) {
		fix = (i * M_PI) / 8;
		val = sin(fix);
		diff = trigOutputs[j] - val;
		EXPECT_TRUE(fabs(diff) < trigTolerance) << "Output exceeds tolerance for " << i << "PI/16: " << val << "/" << trigOutputs[j] << " (" << diff << ")";
		if(j == 8 || j == 0) {
			forward = !forward;
		}
		if(forward) {
			++j;
		}
		else {
			--j;
		}
	}
}

TEST(cosTest, LongMiddle) {
	fixed<long,15,16> fix;
	float val, diff;
	bool forward = true;

	int j = 8;
	for(int i = 0; i < 16; ++i) {
		fix = (i * M_PI) / 8;
		val = cos(fix);
		diff = trigOutputs[j] - val;
		EXPECT_TRUE(fabs(diff) < trigTolerance) << "Output exceeds tolerance for " << i << "PI/16: " << val << "/" << trigOutputs[j] << " (" << diff << ")";
		if(j == 8 || j == 0) {
			forward = !forward;
		}
		if(forward) {
			++j;
		}
		else {
			--j;
		}
	}
}

TEST(cosTest, LongLeft) {
	fixed<long,1,30> fix;
	float val, diff;
	bool forward = true;

	int j = 8;
	for(int i = 0; i < 16; ++i) {
		fix = (i * M_PI) / 8;
		val = cos(fix);
		diff = trigOutputs[j] - val;
		EXPECT_TRUE(fabs(diff) < trigTolerance) << "Output exceeds tolerance for " << i << "PI/16: " << val << "/" << trigOutputs[j] << " (" << diff << ")";
		if(j == 8 || j == 0) {
			forward = !forward;
		}
		if(forward) {
			++j;
		}
		else {
			--j;
		}
	}
}

TEST(cosTest, LongRight) {
	fixed<long,30,1> fix;
	float val, diff;
	bool forward = true;

	int j = 8;
	for(int i = 0; i < 16; ++i) {
		fix = (i * M_PI) / 8;
		val = cos(fix);
		diff = trigOutputs[j] - val;
		EXPECT_TRUE(fabs(diff) < trigTolerance) << "Output exceeds tolerance for " << i << "PI/16: " << val << "/" << trigOutputs[j] << " (" << diff << ")";
		if(j == 8 || j == 0) {
			forward = !forward;
		}
		if(forward) {
			++j;
		}
		else {
			--j;
		}
	}
}

TEST(cosTest, WrappingValues) {
	fixed<long,15,16> fix;
	float val, diff;
	bool forward = true;

	int j = 8;
	for(int i = 0; i < 16000; ++i) {
		fix = (i * M_PI) / 8;
		val = cos(fix);
		diff = trigOutputs[j] - val;
		EXPECT_TRUE(fabs(diff) < trigTolerance) << "Output exceeds tolerance for " << i << "PI/16: " << val << "/" << trigOutputs[j] << " (" << diff << ")";
		if(j == 8 || j == 0) {
			forward = !forward;
		}
		if(forward) {
			++j;
		}
		else {
			--j;
		}
	}
}
